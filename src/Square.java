public class Square extends Shape {

    private int size;

    public Square(int size) {
        this.size = size;
    }

    @Override
    public boolean isEdge(int row, int column) {
        return isInside(row, column) &&  (row == STARTING_INDEX || row == getEndIndex() || column == STARTING_INDEX || column == getEndIndex());
    }

    @Override
    public int getEndIndex() {
        return size-1;
    }

    @Override
    public String edge() {
        return "X";
    }

}
