public abstract class Shape {

    public static final String NON_EDGE = " ";
    public static final int STARTING_INDEX = 0;

    public abstract boolean isEdge(int row, int column);
    public abstract int getEndIndex();
    public abstract String edge();

    public boolean isInside(int row, int column) {
        return row <= getEndIndex() && column <= getEndIndex();
    }

}
