public class Circle extends Shape {

    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public boolean isEdge(int row, int column) {
        int x = Math.abs(row - radius);
        int y = Math.abs(column - radius);
        double distanceFromCenter = Math.sqrt(x*x + y*y);

        return isInside(row, column)  && (int) Math.round(distanceFromCenter) == radius ;
    }

    @Override
    public int getEndIndex() {
        return 2*radius+1;
    }

    @Override
    public String edge() {
        return "*";
    }

}
