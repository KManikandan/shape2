import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.joining;

public class Display {

    public String draw(List<Shape> shapes) {
        int size = shapes.stream().mapToInt(Shape::getEndIndex).max().getAsInt();
        return IntStream.rangeClosed(Shape.STARTING_INDEX, size)
                .mapToObj(row -> row(shapes,row, size))
                .collect(joining("\n"));
    }

    public String row(List<Shape> shapes, int row, int size) {
        return IntStream.rangeClosed(Shape.STARTING_INDEX, size)
                .mapToObj(column -> point(shapes,row, column))
                .collect(joining());
    }

    public String point(List<Shape> shapes, int row, int column) {
        List<String> edges = shapes.stream().filter(s -> s.isEdge(row, column)).map(Shape::edge).collect(Collectors.toList());
        return edges.isEmpty() ? Shape.NON_EDGE : edges.get(edges.size()-1);
    }



}
