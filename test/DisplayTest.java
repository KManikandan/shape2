import org.junit.Test;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class DisplayTest {

    @Test
    public void shouldDrawUnitSquare() {
        Display display = new Display();

        String square = display.draw(asList(new Square(2)));

        assertEquals("XX\nXX", square);
    }

    @Test
    public void shouldDrawSquare() {
        Display display = new Display();

        String square = display.draw(asList(new Square(3)));

        assertEquals("XXX\nX X\nXXX", square);
    }

    @Test
    public void shouldDrawSquareOnTopAnotherSquare() {
        Display display = new Display();

        String square = display.draw(asList(new Square(3), new Square(5)));

        String expected = "XXXXX\n" +
                          "X X X\n" +
                          "XXX X\n" +
                          "X   X\n" +
                          "XXXXX";

        assertEquals(expected, square);
    }

    @Test
    public void shouldDrawCircleOnTopOfSquare() {
        Display display = new Display();

        String square = display.draw(asList(new Square(10), new Circle(4), new Square(4)));

        String expected = "XXXX***XXX\n" +
                          "X**X  ** X\n" +
                          "X* X   **X\n" +
                          "XXXX    *X\n" +
                          "*       *X\n" +
                          "*       *X\n" +
                          "**     **X\n" +
                          "X**   ** X\n" +
                          "X *****  X\n" +
                          "XXXXXXXXXX";

        assertEquals(expected, square);
    }



}
